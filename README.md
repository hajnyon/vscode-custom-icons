# VSCode custom icons

Extended icons for [vscode-icons](https://github.com/vscode-icons/vscode-icons) extension.

![Example custom icons](example.png)

## Installation

Download [files](https://gitlab.com/insensaty/vscode-custom-icons/-/archive/v1.0.0/vscode-custom-icons-v1.0.0.zip) and extract them.

```bash
mkdir /home/<USER>/.config/Code/User/vsicons-custom-icons
cp ./<EXTRACTED_FILES>/icons/* /home/<USER>/.config/Code/User/vsicons-custom-icons
```

Then customize your icons using **user** and/or **project** settings file using `vsicons.associations.files` property. More in [examples](#examples) or [fine tuning](https://github.com/vscode-icons/vscode-icons/wiki/FineTuning).

## Examples

Example setting can be found in `example-settings.json`.

### Custom file extension

Will replace icon of **file.model.ts**

```json
{
    "icon": "blue-6",
    "extensions": [
        "model.ts"
    ],
    "format": "svg"
}
```

### Custom file name

Will replace icon of **index.html**

```json
{
    "icon": "html-green",
    "extensions": [
        "index.html"
    ],
    "filename": true,
    "format": "svg"
}
```

## Available icons

### TypeScript

6 colors in 6 shades.

- blue
- brown-yellow
- green
- purple
- red
- torquoise
- violet

#### Usage

SHADE = <1,6>

`<COLOR>-<SHADE>`

### HTML

9 colors

- blue
- brownish
- dark-blue
- green
- orange
- pink
- purple
- red
- torquoise

#### Usage

`html-<COLOR>`

### CSS

9 colors

- blue
- brownish
- dark-blue
- green
- orange
- pink
- purple
- red
- torquoise

#### Usage

`css-<COLOR>`
